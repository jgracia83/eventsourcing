Change's
========

- 2017 5 4 Install Latest version Symfony 2.8.X
- 2017 5 4 Install Bundle to test prooph/event-store-symfony-bundle, current versión "^0.2.2"
- 2017 5 5 Uninstall Bundle prooph/event-store-symfony-bundle, current versión "^0.2.2" due to only compatible with PHP 7
- 2017 5 5 Install and try Broadway (is a project providing infrastructure and testing helpers for creating CQRS and event sourced applications)
- 2017 5 5 Install Broadway Bundle for Symfony